package universe

import (
	"testing"
)

func TestSetSetsEntry(t *testing.T) {
	var expected byte = 123
	original := universe{}
	changed := original.set(0, expected)
	if !(changed.values[0] == expected) {
		t.Fatalf(`universe.set(0, 123).values[0] = 0x%x, want 0x%x`, changed.values[0], expected)
	}
}
