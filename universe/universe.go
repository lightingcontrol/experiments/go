// Package universe specifies a DMX Universe which contains 256 byte size Values.
package universe

// UniverseSize specifies the number of DMX-Values an Universe contains.
const UniverseSize = 256

type universe struct {
	values [UniverseSize]byte
}

func (u *universe) set(start int, value byte) *universe {
	if start < UniverseSize {
		u.values[start] = value
	}
	return u
}

func (u *universe) add(start int, values []byte) *universe {
	for i := 0; i < len(values); i++ {
		u.set(start+i, values[i])
	}
	return u
}
